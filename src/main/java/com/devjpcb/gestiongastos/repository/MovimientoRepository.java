package com.devjpcb.gestiongastos.repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.ListCrudRepository;

import com.devjpcb.gestiongastos.entity.Movimiento;

public interface MovimientoRepository extends ListCrudRepository<Movimiento, UUID> {
    @Query("select m from Movimiento m where m.fecha between :desde and :hasta order by m.fecha, m.ingreso, m.egreso")
    List<Movimiento> obtenerMovimientos(LocalDate desde, LocalDate hasta);

    @Query("select coalesce(sum(m.ingreso) - sum(m.egreso), 0) from Movimiento m where m.fecha < :fecha")
    BigDecimal obtenerSaldoAnterior(LocalDate fecha);
}
