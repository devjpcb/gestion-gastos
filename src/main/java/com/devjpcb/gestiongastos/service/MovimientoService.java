package com.devjpcb.gestiongastos.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devjpcb.gestiongastos.entity.Movimiento;
import com.devjpcb.gestiongastos.model.FormularioModel;
import com.devjpcb.gestiongastos.model.MovimientoModel;
import com.devjpcb.gestiongastos.repository.MovimientoRepository;

@Service
public class MovimientoService {
    @Autowired
    private MovimientoRepository movimientoRepository;

    public boolean agregarMovimiento(FormularioModel formularioModel) {
        var movimiento = new Movimiento();

        movimiento.setCategoria(formularioModel.getCategoria());
        movimiento.setDescripcion(formularioModel.getDescripcion());
        movimiento.setFecha(formularioModel.getFecha());

        if (formularioModel.getTipo().equals("ingreso")) {
            movimiento.setIngreso(formularioModel.getMonto().abs());
            movimiento.setEgreso(BigDecimal.ZERO);
        } else {
            movimiento.setIngreso(BigDecimal.ZERO);
            movimiento.setEgreso(formularioModel.getMonto().abs());
        }

        movimientoRepository.save(movimiento);

        return true;
    }

    public List<MovimientoModel> obtenerMovimientos(Integer anio, Integer mes) {
        var desde = LocalDate.of(anio, mes, 1);
        var hasta = YearMonth.of(anio, mes).atEndOfMonth();

        var movimientos = movimientoRepository.obtenerMovimientos(desde, hasta);

        if (movimientos.size() > 0) {
            var movimientosModel = movimientos.stream().map(e -> {
                var movimientoModel = new MovimientoModel();
                movimientoModel.setId(e.getId().toString());
                movimientoModel.setCategoria(e.getCategoria());
                movimientoModel.setDescripcion(e.getDescripcion());
                movimientoModel.setFecha(e.getFecha());
                movimientoModel.setIngreso(e.getIngreso());
                movimientoModel.setEgreso(e.getEgreso());
                return movimientoModel;
            }).toList();

            return movimientosModel;
        }

        return new ArrayList<>();
    }

    public BigDecimal obtenerSaldoAnterior(Integer anio, Integer mes) {
        var fecha = LocalDate.of(anio, mes, 1);
        return movimientoRepository.obtenerSaldoAnterior(fecha);
    }

    public void eliminarMovimiento(String id) {
        movimientoRepository.deleteById(UUID.fromString(id));
    }
}
