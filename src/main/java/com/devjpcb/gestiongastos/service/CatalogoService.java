package com.devjpcb.gestiongastos.service;

import java.time.Month;
import java.time.Year;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.devjpcb.gestiongastos.model.CatalogoModel;

@Service
public class CatalogoService {
    public List<CatalogoModel<Integer, String>> ultimosAnios() {
        var lista = new ArrayList<CatalogoModel<Integer, String>>();

        var anioAcual = Year.now().getValue();

        for (var i = anioAcual; i >= (anioAcual - 4); i--) {
            var catalogModel = new CatalogoModel<Integer, String>();
            catalogModel.setCodigo(i);
            catalogModel.setDescripcion(String.valueOf(i));

            lista.add(catalogModel);
        }

        return lista;
    }

    public List<CatalogoModel<Integer, String>> meses() {
        var lista = new ArrayList<CatalogoModel<Integer, String>>();

        for (var i = 1; i <= 12; i++) {
            var mes = Month.of(i);
            var mesNombre = StringUtils.capitalize(mes.getDisplayName(TextStyle.FULL, Locale.getDefault()));

            var catalogModel = new CatalogoModel<Integer, String>();
            catalogModel.setCodigo(i);
            catalogModel.setDescripcion(mesNombre);

            lista.add(catalogModel);
        }

        return lista;
    }
}
