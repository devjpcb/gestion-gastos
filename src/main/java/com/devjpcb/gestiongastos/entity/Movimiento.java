package com.devjpcb.gestiongastos.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
public class Movimiento {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    private String categoria;

    private String descripcion;

    private LocalDate fecha;

    private BigDecimal ingreso;
    
    private BigDecimal egreso;
}
