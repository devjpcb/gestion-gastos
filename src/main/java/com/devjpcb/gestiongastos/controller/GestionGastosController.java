package com.devjpcb.gestiongastos.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.devjpcb.gestiongastos.model.CatalogoModel;
import com.devjpcb.gestiongastos.model.FormularioModel;
import com.devjpcb.gestiongastos.model.MovimientosModel;
import com.devjpcb.gestiongastos.service.CatalogoService;
import com.devjpcb.gestiongastos.service.MovimientoService;

import jakarta.servlet.http.HttpServletResponse;

@Controller
public class GestionGastosController {
    @Autowired
    private CatalogoService catalogoService;

    @Autowired
    private MovimientoService movimientoService;

    @ModelAttribute("anios")
    public List<CatalogoModel<Integer, String>> anios() {
        return catalogoService.ultimosAnios();
    }

    @ModelAttribute("meses")
    public List<CatalogoModel<Integer, String>> meses() {
        return catalogoService.meses();
    }

    @GetMapping("/index")
    public String index() {
        return "index";
    }

    @GetMapping("/formulario")
    public String formulario(Model model) {
        var formularioModel = new FormularioModel();

        model.addAttribute(formularioModel);

        return "formulario";
    }

    @GetMapping("/movimientos")
    public String movimientos(Model model, Integer anio, Integer mes, boolean incluirSaldoAnterior) {
        if (anio == null || mes == null) {
            var now = LocalDate.now();
            anio = now.getYear();
            mes = now.getMonthValue();
            incluirSaldoAnterior = true;
        }

        var saldoAnterior = movimientoService.obtenerSaldoAnterior(anio, mes);

        var movimientos = movimientoService.obtenerMovimientos(anio, mes);

        var movimientosModel = new MovimientosModel();
        movimientosModel.setAnio(anio);
        movimientosModel.setMes(mes);
        movimientosModel.setIncluirSaldoAnterior(incluirSaldoAnterior);
        movimientosModel.setSaldoAnterior(saldoAnterior);
        movimientosModel.getMovimientos().addAll(movimientos);

        model.addAttribute(movimientosModel);

        return "movimientos";
    }

    @PostMapping("/agregar-movimiento")
    public String agregarMovimiento(Model model, HttpServletResponse response, FormularioModel formularioModel) {
        if (movimientoService.agregarMovimiento(formularioModel)) {
            formularioModel = new FormularioModel();
        }

        model.addAttribute(formularioModel);

        response.setHeader("HX-Trigger-After-Settle", "recagar");

        return "formulario";
    }

    @GetMapping("/seleccionar-movimientos")
    public String seleccionarMovimientos(Model model, @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fecha) {
        return movimientos(model, fecha.getYear(), fecha.getMonthValue(), true);
    }

    @DeleteMapping("/eliminar-movimiento")
    public String eliminarMovimiento(Model model, String id, Integer anio, Integer mes, boolean incluirSaldoAnterior) {
        movimientoService.eliminarMovimiento(id);
        return movimientos(model, anio, mes, incluirSaldoAnterior);
    }
}
