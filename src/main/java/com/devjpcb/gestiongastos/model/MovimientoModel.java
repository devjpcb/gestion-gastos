package com.devjpcb.gestiongastos.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.Data;

@Data
public class MovimientoModel {
    private String id;

    private String categoria;

    private String descripcion;

    private LocalDate fecha;

    private BigDecimal ingreso;
    
    private BigDecimal egreso;
}
