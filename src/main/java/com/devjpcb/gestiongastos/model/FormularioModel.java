package com.devjpcb.gestiongastos.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class FormularioModel {
    private String categoria;

    private String descripcion;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate fecha = LocalDate.now();

    private BigDecimal monto;
    
    private String tipo = "egreso";
}
