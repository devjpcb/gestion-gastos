package com.devjpcb.gestiongastos.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
public class MovimientosModel {
    private int anio;

    private int mes;

    private boolean incluirSaldoAnterior;

    private BigDecimal saldoAnterior;

    @Setter(AccessLevel.NONE)
    private List<MovimientoModel> movimientos = new ArrayList<>();

    public BigDecimal getSaldoAnteriorAbs() {
        return saldoAnterior.abs();
    }

    public BigDecimal getTotalIngreso() {
        var ingreso = movimientos.stream().map(e -> e.getIngreso()).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));

        if (incluirSaldoAnterior && saldoAnterior.compareTo(BigDecimal.ZERO) > 0) {
            ingreso = ingreso.add(saldoAnterior);
        }

        return ingreso;
    }

    public BigDecimal getTotalEgreso() {
        var egreso = movimientos.stream().map(e -> e.getEgreso()).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));

        if (incluirSaldoAnterior && saldoAnterior.compareTo(BigDecimal.ZERO) < 0) {
            egreso = egreso.add(saldoAnterior.abs());
        }

        return egreso;
    }

    public BigDecimal getSaldo() {
        return getTotalIngreso().subtract(getTotalEgreso());
    }
}
