package com.devjpcb.gestiongastos.model;

import lombok.Data;

@Data
public class CatalogoModel<K, V> {
    private K codigo;
    
    private V descripcion;
}
